// @flow

// Haven't bothered cleaning any of this up. Would use lodash/ramda or something in production
export const range = to => new Array(to).fill().map((_, i) => i);

export const zip = (arr1, arr2) =>
  range(Math.min(arr1.length, arr2.length)).map(i => [arr1[i], arr2[i]]);

export const reduce = f => init => arr => arr.reduce(f, init);

export const last = arr => arr[arr.length - 1];

export const first = arr => arr[0];

export const scan = f => init => arr =>
  arr.reduce((p, c) => [...p, f(last(p), c)], [init]);

export const filter = f => arr => arr.filter(f);

export const map = f => arr => arr.map(f);

export const mapObj = f => o => merge(Object.keys(o).map(k => ({ [k]: o[k] })));

export const merge = arr => Object.assign({}, ...arr);

export const splitAt = f => arr =>
  arr.reduce((p, c, i) => {
    const fst = p.slice(0, -1);
    const lst = last(p) || [];
    const next = f(c, i) ? [lst, [c]] : [[...lst, c]];

    return [...fst, ...next];
  }, []);

export const pipe = (...fns) =>
  fns.reduce((g, f) => (...args) => f(g(...args)));

export const fail = reason => {
  throw new Error(reason);
};
