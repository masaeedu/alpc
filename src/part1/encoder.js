// @flow

import { pipe, range, map, fail } from "../utils";

// Maximum number that can fit in a number of bits
export const fill = bits => (1 << bits) - 1;

// Bit length of an unsigned integer
export const bitLength = n => n.toString(2).length;

// i-th most significant bit range of length l from number n
export const slice = (i, l, n) => ((fill(l) << (i * l)) & n) >> (i * l);

// Specified length bit ranges from unsigned integer, ordered most significant to least
export const pack = sliceLength => n => {
  const numOfSlices = Math.ceil(bitLength(n) / sliceLength);
  const indices = range(numOfSlices);
  return indices.reverse().map(i => slice(i, sliceLength, n));
};

// Unsigned integer from specified length bit ranges, ordered most significant to least
export const unpack = sliceLength => slices => {
  return slices.reverse().reduce((p, c, i) => p | (c << (i * sliceLength)), 0);
};

// Encodes a number smaller than a byte into a 2 character string
export const byteToHex = n => n.toString(16).padStart(2, "0");

// Maximum positive number that fits in a particular signed length
export const signedMax = signedLength => fill(signedLength - 1);

// Mimimum negative number that fits in a particular signed length
export const signedMin = signedLength => -signedMax(signedLength) - 1;

// Encoder that:
// - accepts an unsigned integer
// - chops it into bit ranges of specified length (no larger than a byte)
// - represents each bit range as a 2 char hex string
export const createEncoder = chunkLength => {
  if (chunkLength > 8) fail(`Chunk length ${chunkLength} exceeds a byte`);

  const join = arr => arr.join("");
  const encode = pipe(pack(chunkLength), map(byteToHex), join);

  const decode = pipe(code => parseInt(code, 16), pack(8), unpack(chunkLength));

  return { encode, decode };
};

// Provide an encoding for signed integers
const enforceSize = bits => n =>
  bitLength(n) > bits ? fail(`${n} exceeds bit length ${bits}`) : n;

export const signed = signedLength => ({ encode, decode }) => {
  const check = enforceSize(signedLength);

  const offset = -signedMin(signedLength);
  const inc = n => n + offset;
  const dec = n => n - offset;

  return {
    encode: pipe(inc, check, encode),
    decode: pipe(decode, check, dec)
  };
};

// Provide fixed-length encoding
const enforceLength = l => s =>
  s.length > l ? fail(`"${s}" exceeds expected length ${l}`) : s;

export const padded = codeLength => ({ encode, decode }) => {
  const check = enforceLength(codeLength);
  const pad = s => s.padStart(codeLength, "0");

  return {
    encode: pipe(encode, pad, check),
    decode: pipe(check, decode)
  };
};

// ALPC-specific encoder
const signedLength = 14;
const chunkLength = signedLength / 2;

const createALPCEncoder = pipe(createEncoder, signed(signedLength), padded(4));

export const alpcEncoder = createALPCEncoder(chunkLength);
