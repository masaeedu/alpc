// @flow

import { pipe, splitAt, map, filter, merge, fail } from "../utils";
import { alpcEncoder } from "../part1/encoder";

const { decode } = alpcEncoder;

// Transformations from argument arrays to structured objects
// prettier-ignore
const opParsers = {
  "F0": ([])             => ({ op: "CLR" }),
  "80": ([down])         => ({ op: "PEN", down }),
  "A0": ([r, g, b, a])   => ({ op: "CO", r, g, b, a }),
  "C0": ([...movements]) => ({
    op: "MV",
    movements: splitAt(everyNth(2))(movements)
  })
};

const isOpCode = hexbyte => parseInt(hexbyte, 16) & 10000000;

const everyNth = n => (_, i) => i > 0 && i % n === 0;

export const parse = pipe(
  // Split string to char array,
  s => s.split(""),

  // Split at every second character
  splitAt(everyNth(2)),

  // Convert 2-character arrays to strings
  map(arr => arr.join("")),

  // Split at every opcode
  splitAt(isOpCode),

  // Ignore empty command arrays
  filter(arr => !!arr.length),

  // Convert each command array to an object
  map(raw => {
    const opcode = raw[0];

    const argBytes = raw.slice(1);
    if (argBytes.length % 2 !== 0)
      fail("Uneven number of bytes in argument list");

    const argBytePairs = splitAt(everyNth(2))(argBytes);
    const args = argBytePairs.map(([hi, lo]) => decode(hi + lo));

    // If there is no parser for this opcode, just
    // return the opcode and decoded arguments
    const parser = opParsers[opcode];
    if (!parser) return { op: "UNKNOWN", opcode, args };

    return parser(args);
  })
);
