// @flow

import {
  pipe,
  splitAt,
  map,
  mapObj,
  filter,
  merge,
  reduce,
  scan,
  range,
  first,
  last
} from "../utils";
import { sum, eq, clipLB, clipTrajectory, inRect } from "./geometry";

export const initState = () => ({
  position: [0, 0],
  down: false,
  color: { r: 0, g: 0, b: 0, a: 255 }
});

// Transformations from (current state, command) to a sequence of new target states
// prettier-ignore
export const stateMachine = {
    CLR: (s, c) => [initState()],
    PEN: (s, c) => [{ ...s, down: c.down > 0 }],
    CO:  (s, c) => [{ ...s, color: c }],
    MV:  (s, c) => {
      const { movements } = c;

      const positions = scan(sum)(s.position)(movements);
      const states = positions.map(p => ({ ...s, position: p }));

      return states.slice(1);
    }
  };

const bounds = [-8192, 8191, -8192, 8191];
const inBounds = inRect(bounds);
const clip = clipLB(bounds);

// An effect interpreter is a mapping of command names to effect thunks
// An effect thunk takes a sequence of target states and traverses it by performing arbitrary side effects

// This interpreter emits strings representing instructions to a "turtle graphics" style renderer
export const turtle = ({ emit }) => ({
  CLR: () => {
    emit("CLR");
  },

  // The instructions leave ambiguous what should happen when the current
  // position is out of bounds and the pen is lifted/dropped.

  // I assume no PEN commands should be emitted until we return to within
  // bounds.
  PEN: (s, c, [{ down }]) => {
    if (inBounds(s.position)) emit(`PEN ${down ? "DOWN" : "UP"}`);
  },

  // No requirement is expressed to skip color changes when the current
  // color already matches the target state, so two identical color
  // commands will result in two lines emitted
  CO: (s, c, [{ color: { r, g, b, a } }]) => {
    emit(`CO ${r} ${g} ${b} ${a}`);
  },

  // The instructions leave ambiguous what should happen when the pen is
  // lifted and the final target point is out of bounds.

  // E.g. if we are at (0, 0) and we receive the move command
  // [(5000, 200), (5000, 0)], which of the following do we emit?

  // - MV (10000, 200)
  // - MV (8191, 200)
  // - MV (8191, 164)

  // The last two options assume that we want to clip even when the pen is
  // lifted. However our intercept with the edge depends on whether the
  // line segment to clip is between the current state and the final position,
  // or between the last two points in the command list.

  // I assume the second option is what you want.
  MV: (s, c, states) => {
    const serialize = ([x, y]) => `(${Math.round(x)}, ${Math.round(y)})`;
    const emitMoves = points => emit(`MV ${points.map(serialize).join(" ")}`);

    const positions = [s.position, ...states.map(x => x.position)];
    const trajs = clipTrajectory(clip)(positions);

    // If you're just moving the pen around out of bounds, do nothing
    if (!trajs.length) return;

    // If the pen isn't down, just move straight to the destination
    const destination = last(last(trajs));
    if (!s.down) {
      emitMoves([destination]);
      return;
    }

    // Otherwise, draw all internal trajectories
    const [source, target] = [first(positions), last(positions)];

    for (let i = 0; i < trajs.length; i++) {
      const traj = trajs[i];

      const [first, ...rest] = traj;

      // Move to starting point and put pen down
      // (unless we're just starting and the source is inside)
      if (i > 0 || !inBounds(source)) {
        emitMoves([first]);
        emit("PEN DOWN");
      }

      emitMoves(rest);

      // Lift pen
      // (unless we're done and the target is inside)
      if (i < trajs.length - 1 || !inBounds(target)) {
        emit("PEN UP");
      }
    }
  }
});

export const interpret = ({
  interpreter,
  stateMachine
}) => commands => state => {
  let currState = state;

  for (let c of commands) {
    // Ignore unknown commands
    if (!stateMachine[c.op]) continue;

    // Compute state transitions
    const states = stateMachine[c.op](currState, c);

    // Perform effects
    interpreter[c.op](currState, c, states);

    // State of the world should now be final target state
    currState = last(states);
  }
};
