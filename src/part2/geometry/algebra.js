// @flow

// Vector algebra stuff
export const scale = (k, [x, y]) => [k * x, k * y];
export const dot = ([x1, y1], [x2, y2]) => x1 * x2 + y1 * y2;
export const sum = ([x1, y1], [x2, y2]) => [x1 + x2, y1 + y2];
export const diff = (v1, v2) => sum(v1, scale(-1, v2));
export const norm = ([x1, y1], [x2, y2]) => {
  const dx = x2 - x1;
  const dy = y2 - y1;

  return dx === 0 ? [-dy, 0] : [-dy / dx, 1];
};
export const eq = ([x1, y1], [x2, y2]) => x1 === x2 && y1 === y2;
