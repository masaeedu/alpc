// @flow

import { range, pipe } from "../../utils";
import { norm, dot, diff, scale } from "./algebra";

// Outer norms of a convex polygon's edges
// Expects:
// - vertices of convex bounding polygon in CCW order
export const polyNorms = vertices => {
  const n = vertices.length;
  const indices = range(n);

  return indices.map(i => {
    // Pick three consecutive vertices
    const [v1, v2, v3] = range(3).map(v => vertices[(i + v) % n]);

    // Compute one of two possible norms using vertices
    const aNorm = norm(v1, v2);

    // Use third vertex to ensure norm points outwards
    return dot(aNorm, diff(v3, v1)) > 0 ? scale(-1, aNorm) : aNorm;
  });
};

export const inPoly = (vertices, norms = polyNorms(vertices)) => point => {
  const n = vertices.length;
  const indices = range(n);

  // Point is inside poly if it's inside every edge
  return indices.every(i => {
    const v = vertices[i];
    const n = norms[i];

    return dot(n, diff(point, v)) <= 0;
  });
};

export const rectangleVertices = ([minX, maxX, minY, maxY]) => [
  [minX, minY],
  [maxX, minY],
  [maxX, maxY],
  [minX, maxY]
];

export const inRect = pipe(rectangleVertices, inPoly);
