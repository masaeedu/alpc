// @flow
import { zip, last } from "../../../utils";
import { eq } from "../algebra";

// Expects:
// - a clip function for a convex polygon
// - a sequence of points representing a trajectory
// Returns:
// - array of trajectories internal to polygon
export const clipTrajectory = clip => points => {
  const legs = zip(points, points.slice(1));

  return legs.reduce((trajs, [p1, p2]) => {
    const clipped = clip(p1, p2);

    // Ignore legs totally outside the polygon
    if (clipped === undefined) return trajs;

    // Determine whether the leg enters/exits the polygon
    const [pc1, pc2] = clipped;
    const entering = !eq(p1, pc1);
    const exiting = !eq(p2, pc2);

    const curr = last(trajs);

    // If this leg enters the polygon, or this is the first
    // point, emit a new trajectory
    if (entering || !curr) return [...trajs, [pc1, pc2]];

    // Otherwise, continue current trajectory
    const prev = trajs.slice(0, -1);
    return [...prev, [...curr, pc2]];
  }, []);
};
