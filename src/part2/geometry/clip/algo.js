// @flow

import { range, pipe } from "../../../utils";
import { polyNorms, rectangleVertices } from "../utils";
import { sum, diff, dot, scale } from "../algebra";

// Cyrus-Beck clip
// Expects:
// - vertices of convex bounding polygon in CCW order
// - two points
// Returns:
// - clipped line segment, or undefined if the segment lies outside the polygon
export const clipCB = (vertices, norms = polyNorms(vertices)) => (p1, p2) => {
  const seg = diff(p2, p1);

  const n = vertices.length;
  const indices = range(n);

  // Determine "inside" interval of line segment by testing against every edge
  const t = indices.reduce(
    (t, i) => {
      if (t === undefined) return;

      const [t1, t2] = t;
      const v = vertices[i];
      const n = norms[i];

      const segToEdge = diff(p1, v);

      const num = dot(n, segToEdge);
      const den = dot(n, seg);

      const ratio = -num / den;

      // Segment enters edge
      if (den < 0 && ratio > t1) return [ratio, t2];

      // Segment exits edge
      if (den > 0 && ratio < t2) return [t1, ratio];

      // Segment is parallel, and on the wrong side
      if (den === 0 && num > 0) return undefined;

      return [t1, t2];
    },
    [0, 1]
  );

  if (t === undefined) return;

  const [t1, t2] = t;

  // If the parametric boundaries are flipped around the segment doesn't intercept the polygon at all
  if (t1 > t2) return;

  return [sum(p1, scale(t1, seg)), sum(p1, scale(t2, seg))];
};

// Liang-Barsky clip
export const clipLB = pipe(rectangleVertices, clipCB);
