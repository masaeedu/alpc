// @flow

import readline from "readline";

import { pipe } from "../utils";
import { parse } from "../part2/parser";
import {
  interpret,
  turtle,
  stateMachine,
  initState
} from "../part2/interpreter";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const emit = s => console.log(s + ";");
const interpretF = interpret({ interpreter: turtle({ emit }), stateMachine });
const state = initState();

rl.on("line", l => {
  interpretF(parse(l))(state);
});
