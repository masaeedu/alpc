// @flow

import * as yargs from "yargs";
import readline from "readline";

import { pipe } from "../utils";
import { alpcEncoder } from "../part1/encoder";

const { encode, decode } = alpcEncoder;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const log = console.log.bind(console);
yargs
  .command(
    "encode",
    "encode lines from stdin to hex strings",
    () => {},
    argv => rl.on("line", pipe(parseInt, encode, log))
  )
  .command(
    "decode",
    "decode lines from stdin to integers",
    () => {},
    argv => rl.on("line", pipe(decode, log))
  )
  .demandCommand()
  .help().argv;
