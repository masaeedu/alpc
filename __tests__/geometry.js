import test from "ava";

import {
  polyNorms,
  clipLB,
  clipTrajectory,
  scale,
  sum,
  rectangleVertices
} from "../src/part2/geometry";
import { map, pipe } from "../src/utils";

test("computing norms works", t => {
  const result = polyNorms([[0, 0], [1, 0], [1, 1], [0, 1]]);
  t.snapshot(result);
});

{
  const clip = clipLB([0, 1, 0, 1]);

  test("clipping a line that crosses the polygon shortens it", t => {
    const result = clip([-2, 0.5], [2, 0.5]);
    t.snapshot(result);
  });

  test("clipping a line that is inside the polygon does nothing", t => {
    const result = clip([0.25, 0.5], [0.75, 0.5]);
    t.snapshot(result);
  });

  test("clipping a line outside the polygon produces undefined", t => {
    const result = clip([0.25, -1], [0.75, -1]);
    t.snapshot(result);
  });

  const scaleT = k => map(p => scale(k, p));
  const shiftT = p1 => map(p2 => sum(p1, p2));
  const clipT = clipTrajectory(clip);

  const template = rectangleVertices([0, 1, 0, 1]);

  test("clipping a trajectory that is entirely outside returns empty", t => {
    const traj = shiftT([2, 0])(template);
    const result = clipT(traj);

    t.snapshot(result);
  });

  test("clipping a trajectory that is entirely inside returns it unchanged", t => {
    const traj = pipe(scaleT(0.5), shiftT([0.25, 0.25]))(template);
    const result = clipT(traj);

    t.snapshot(result);
  });

  test("clipping a trajectory clips appropriate edges", t => {
    const traj = pipe(scaleT(0.5), shiftT([-0.25, 0.25]))(template);
    const result = clipT(traj);

    t.snapshot(result);
  });
}
