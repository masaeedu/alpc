import test from "ava";

import { parse } from "../src/part2/parser";

test("parsing works", t => {
  const input = "F0A04000417F4000417FC040004000804001C05F205F20804000";

  const result = parse(input);
  t.snapshot(result);
});
