import test from "ava";
import * as jsc from "jsverify";

import { pipe } from "../src/utils";
import {
  pack,
  unpack,
  fill,
  bitLength,
  signedMin,
  signedMax,
  createEncoder,
  signed,
  padded
} from "../src/part1/encoder";

// Ensure that combine is inverse of chop
test("pack is inverse of unpack", t => {
  const prop = jsc.forall(jsc.nat, n =>
    jsc.forall(
      jsc.integer(1, bitLength(n)),
      l => n === pipe(pack(l), unpack(l))(n)
    )
  );
  t.notThrows(() => jsc.assert(prop));
});

// Encoder tests
const signedLength = 14;
const chunkLength = signedLength / 2;

// Get min and max number for the problem statement
const min = signedMin(signedLength);
const max = signedMax(signedLength);

test("boundaries are correct", t => {
  t.is(max, 8191);
  t.is(min, -8192);
});

// Domain for random testing
const domain = jsc.integer(max);

// Create signed, padded encoder
const ce = pipe(createEncoder, signed(signedLength), padded(4));

// Check encoder encodes/decodes various points in domain idempotently
const { encode, decode } = ce(chunkLength);

const recode = pipe(encode, decode);

test("encoder is idempotent", t => {
  t.is(recode(min), min);
  t.is(recode(max), max);
  t.is(recode(0), 0);

  const prop = jsc.forall(domain, n => n === recode(n));
  t.notThrows(() => jsc.assert(prop));
});

// Check that encoder pads correctly
test("padding is correct", t => {
  t.is(encode(min).length, 4);
  t.is(encode(max).length, 4);
  t.is(encode(0).length, 4);

  const prop = jsc.forall(domain, n => encode(n).length === 4);
  t.notThrows(() => jsc.assert(prop));
});

// Reproduce table from PDF
const inputs = [0, -8192, 8191, 2048, -4096, -6907, 2688];

test("misc inputs", t => {
  const table = inputs
    .map(n => [n, encode(n), recode(n)])
    .map(([n, c, rn]) => `${n} | ${c} | ${rn}`)
    .join("\n");
  t.snapshot(table);
});
