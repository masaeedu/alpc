import test from "ava";

import { parse } from "../src/part2/parser";
import { interpret, turtle, stateMachine, initState } from "../src/part2/interpreter";
import { rectangleVertices } from "../src/part2/geometry";

const interpretToString = (commands, state = initState()) => {
  const lines = [];
  const emit = line => lines.push(line);

  const interpreter = turtle({ emit });

  interpret({ interpreter, stateMachine })(commands)(state);

  return lines.map(l => l + ";").join("\n");
};

// Test pen up vs down
{
  const clrCmd = { op: "CLR" };
  const penCmd = { op: "PEN", down: 1 };
  const moveCmd = {
    movements: [[10, 10], [5, -5]],
    op: "MV"
  };

  test("interpreting movement when pen is up goes straight to target", t => {
    const result = interpretToString([clrCmd, moveCmd]);
    t.snapshot(result);
  });

  test("interpreting movement when pen is down visits all points", t => {
    const result = interpretToString([clrCmd, penCmd, moveCmd]);
    t.snapshot(result);
  });
}

// Test samples from PDF
{
  const inputs = [
    // Green line
    "F0A04000417F4000417FC040004000804001C05F205F20804000",

    // Blue square
    "F0A040004000417F417FC04000400090400047684F5057384000804001C05F204000400001400140400040007E405B2C4000804000",

    // Straight line clipping
    "F0A0417F40004000417FC067086708804001C0670840004000187818784000804000",

    // Angled line clipping
    "F0A0417F41004000417FC067086708804001C067082C3C18782C3C804000"
  ];

  for (let input of inputs) {
    test(`interpreting ${input} works`, t => {
      const result = interpretToString(parse(input));
      t.snapshot(result);
    });
  }
}

// Test some edge cases
{
  const moveInASquare = [
    {
      op: "MV",
      movements: rectangleVertices([0, 1, 0, 1])
    }
  ];

  test("moving around inside bounds doesn't cause pen effects", t => {
    const s = {
      position: [0, 0],
      down: true
    };
    const result = interpretToString(moveInASquare, s);

    t.snapshot(result);
  });

  test("moving around outside bounds doesn't do anything", t => {
    const s = {
      position: [10000, 0],
      down: false
    };
    const result = interpretToString(moveInASquare, s);

    t.snapshot(result);
  });

  test("putting the pen down when outside only takes effect after entering", t => {
    const s = {
      position: [10000, 0],
      down: false
    };

    const commands = [
      { op: "PEN", down: 1 },
      { op: "MV", movements: [[-10000, 0]] }
    ];
    const result = interpretToString(commands, s);

    t.snapshot(result);
  });
}
