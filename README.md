# Art & Logic Programming Challenge

## Part 1

### Instructions

![instructions](./instructions/instructions.png)

### Layout

The implementation code is in `src/part1`. The executable entrypoint is `src/bin/part1.js`.

AVA-based tests are in `__tests__`. Results from snapshot based tests are in the `__tests__/__snapshots__` subdirectory.

Sample inputs and outputs as instructed in the PDF are in the `samples` folder. You can rebuild the outputs for yourself by running `cd samples && ./build.sh`, after following the steps in the Setup section below.

### Setup

```bash
cd path/to/untarred/project
# If the JS filename extensions have been munged to .javascript, fix those
shopt -s globstar && rename s/.javascript/.js/ {src,__tests__}/**/*.javascript
yarn
```

### Usage

Executable entrypoint can be invoked using `yarn encoder`. It expects a single command: `encode` or `decode`, and will process input line-by-line from stdin, emitting results on stdout.

Example usage: `echo "-8192" | yarn --silent encoder encode`, which outputs `0000`.

## Part 2

### Layout, Setup

Similar to Part 1, `s/part1/part2/`.

### Usage

Executable entrypoint can be invoked using `yarn interpreter`. Similarly to part 1, accepts inputs line-by-line from stdin and emits results on stdout.

Example usage:

```bash
$ echo F0A04000417F4000417FC040004000804001C05F205F20804000 | yarn --silent interpreter
CLR;
CO 0 255 0 255;
MV (0, 0);
PEN DOWN;
MV (4000, 4000);
PEN UP;
```
