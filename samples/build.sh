tasks=( encode decode )
for task in "${tasks[@]}"
do
	cat ./input_$task.txt | yarn --silent encoder $task > output_$task.txt
done